# JS Mocks

I want this repository to be a working example of how to design a software system, using test-driven development.  

It will follow the lead of a ThoughtWorks paper I read [Mock Roles, not Objects](http://jmock.org/oopsla2004.pdf).  
The subject will be a DVCS system that lists repositories and fetches them from a remote source. This source could be 
GitHub, GitLab or something similar. I will not go into the implementation of the Git*-APIs. This will be "hidden" 
behind an interface class.

The goal is to test-drive the creation of a system that has the following functions:

- It lists repositories.
- It caches responses.
- It needs some kind of authorization.

Every commit is supposed to serve as a single step in the creation of the system. If you "replay" the Git history, you 
can follow along the creation of this system.

As a practical example this repository uses Mocks to help design the system.

In its current state (Spring 2019), this system is not fully developed yet. 
